# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import unittest

class BlogPost2Single(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/"
    
    @classmethod
    def tearDownClass(self):
        self.driver.quit()
        
    def setUp(self):
        self.driver.get(self.base_url + "/blog/")
        
    def test_blog_post2_single(self):
        driver = self.driver
        
        #Chceck if the link is present
        try: self.assertEqual("Sample Post2", driver.find_element_by_link_text("Sample Post2").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def test_kolejny(self):
        driver = self.driver
        
        #Check if the image is present
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "img.img-rounded.wp-post-image"))
        
    def test_kolejny_2(self):
        driver = self.driver
        
        #Check if link is present on the page 
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "p.pull-right.hidden-xs > a.btn.btn-primary"))
    
    def test_kolejny_3(self):
        driver = self.driver

        #Check if link has correct label
        self.assertEqual("Read more", driver.find_element_by_css_selector("p.pull-right.hidden-xs > a.btn.btn-primary").text)
    
    def test_kolejny_4(self):
        driver = self.driver

        #Open Post 2 by clicking the picture
        driver.find_element_by_css_selector("img.img-rounded.wp-post-image").click()
    
    def test_kolejny_5(self):
        driver = self.driver

        #Look for specific text on the page and verify it
        self.assertEqual("Sample Post2", driver.find_element_by_css_selector("h1.entry-title").text)
            
    def test_kolejny_6(self):
        driver = self.driver
        
        self.assertEqual(u"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using ‘lorem ipsum’ is that it has a more-or-less normal distribution of letters, as opposed to using ‘This here is content’ or ‘Osama Bin Laden wanted dead or alive’, making it look like readable text and there also has been many examples what has happened if someone has forgotten to change the dummy text for the final copywrite text.\n In Finland there was couple of years ago this one bookshop banner in public internet with ‘Buying a book is as easy as hitting a child’ text in it.", driver.find_element_by_css_selector("p").text)
       
    def test_kolejny_7(self):
        driver = self.driver

        self.assertTrue(self.is_element_present(By.LINK_TEXT, "permalink"))

    def test_kolejny_8(self):
        driver = self.driver

        self.assertTrue(self.is_element_present(By.LINK_TEXT, "Sample Post"))

    def test_kolejny_9(self):
        driver = self.driver
        
        self.assertTrue(self.is_element_present(By.XPATH, "//main[@id='main']/p/span"))
        
        #Close the browser
        driver.close()
        
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        pass
#        self.driver.quit()
#        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()