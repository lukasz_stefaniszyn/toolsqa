package shared;

import java.util.LinkedList;
import datamodel.*;

public interface DataDrivenInterface {

	public TestCase getTextCaseById(Long id);
	public LinkedList<TestCase> getAllTestCase();
	

}
