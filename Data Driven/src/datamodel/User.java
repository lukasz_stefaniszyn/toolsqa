package datamodel;
/**
 * Data object representive user parameters which will be used in Test Case
 * @param alpha - variable used by Fidelity 
 */
import dict.UserTypeEnum;

public class User {
	private Long id;
	private String name;
	private String ssn;
	private UserTypeEnum type;
	private String  requeirment;
	
	public User() {
	}
	
	public User(Long id, String alpha, String ssn, UserTypeEnum type,
			String requeirment) {
		super();
		this.id = id;
		this.name = alpha;
		this.ssn = ssn;
		this.type = type;
		this.requeirment = requeirment;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public UserTypeEnum getType() {
		return type;
	}
	public void setType(UserTypeEnum type) {
		this.type = type;
	}
	public String getRequeirment() {
		return requeirment;
	}
	public void setRequeirment(String requeirment) {
		this.requeirment = requeirment;
	}
	
	
	
}
