package datamodel;

import java.util.LinkedList;

/**
 * Data object representive one test case for one environment
 * and one user and one result
 * @author KPOJASEK
 *
 */
public class TestCase {
	private Environment environnment;
	private User user;
	private LinkedList<Content> content;
	
	public TestCase() {
		content = new LinkedList<Content>();
	}

	public Environment getEnvironnment() {
		return environnment;
	}

	public void setEnvironnment(Environment environnment) {
		this.environnment = environnment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LinkedList<Content> getContent() {
		return content;
	}

	public void setContent(LinkedList<Content> content) {
		this.content = content;
	}
}
