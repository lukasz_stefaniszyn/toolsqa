package datamodel;

import dict.BrowserTypeEnum;
import dict.DeviceEnum;
import dict.ResolutionEnum;
import dict.SystemTypeEnum;

/** 
 * Environment is a transfer object class representive where test is run.
 * Thats mean : OS, browser, resolution etc.
 * @author KPOJASEK
 *
 */
public class Environment {
	private BrowserTypeEnum browserType;
	private SystemTypeEnum systemType;
	private DeviceEnum deviceType;
	private ResolutionEnum resolution;
	
	public Environment() {
	}

	public BrowserTypeEnum getBrowserType() {
		return browserType;
	}

	public void setBrowserType(BrowserTypeEnum browserType) {
		this.browserType = browserType;
	}

	public SystemTypeEnum getSystemType() {
		return systemType;
	}

	public void setSystemType(SystemTypeEnum systemType) {
		this.systemType = systemType;
	}

	public DeviceEnum getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceEnum deviceType) {
		this.deviceType = deviceType;
	}

	public ResolutionEnum getResolution() {
		return resolution;
	}

	public void setResolution(ResolutionEnum resolution) {
		this.resolution = resolution;
	}
	
}
