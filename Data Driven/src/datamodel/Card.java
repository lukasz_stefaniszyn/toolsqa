package datamodel;
/**
 * Card is a transfer data object representive card object which is expected in summary object.
 * @author KPOJASEK
 *
 */
public class Card extends Content {
	private String name;
	private String order;
	
	public Card() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
	
}
