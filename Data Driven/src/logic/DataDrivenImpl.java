package logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import org.supercsv.io.CsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import datamodel.TestCase;
import shared.DataDrivenInterface;

public class DataDrivenImpl implements DataDrivenInterface {
	private String path;
	private String filename = "test.csv";
	private String seperate = "|";
	private CsvBeanReader reader;
	private String [] header;
	private String [] subHeader;


	public DataDrivenImpl(String path, String filename) {
		this.filename = filename;
		this.path = path;
		try {
			reader = new CsvBeanReader(new FileReader(filename), CsvPreference.STANDARD_PREFERENCE);
			header = TestCaseBean.HEADER;
			TestCaseBean testCase = reader.read(TestCaseBean.class, header);
            System.out.println(testCase.toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public TestCase getTextCaseById(Long id) {
		return null;
	}

	@Override
	public LinkedList<TestCase> getAllTestCase() {
		return null;
	}

}
