package logic;

public class TestCaseBean {
	
	public final static String [] HEADER = {"name" ,  "SSN" ,  "type" ,  "req" ,  "device" ,  "OS" ,  "browser" ,  "ver" ,  "display" ,  "resol" ,  "accSelector" ,  "cards" ,  "mainMenu" ,  "tabs"};
	/** Who **/
	private String name;
	private String SSN;
	private String type;
	private String req;
	/** Where **/
	private String device;
	private String OS;
	private String browser;
	private String ver;
	private String display;
	private String resol;
	/** What **/
	private String accSelector;
	private String cards;
	private String mainManu;
	private String tabs;
	
	
	
	public TestCaseBean(String name, String sSN, String type, String req,
			String device, String oS, String browser, String ver,
			String display, String resol, String accSelector, String cards,
			String mainManu, String tabs) {
		super();
		this.name = name;
		SSN = sSN;
		this.type = type;
		this.req = req;
		this.device = device;
		OS = oS;
		this.browser = browser;
		this.ver = ver;
		this.display = display;
		this.resol = resol;
		this.accSelector = accSelector;
		this.cards = cards;
		this.mainManu = mainManu;
		this.tabs = tabs;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSSN() {
		return SSN;
	}
	public void setSSN(String sSN) {
		SSN = sSN;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReq() {
		return req;
	}
	public void setReq(String req) {
		this.req = req;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getOS() {
		return OS;
	}
	public void setOS(String oS) {
		OS = oS;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getVer() {
		return ver;
	}
	public void setVer(String ver) {
		this.ver = ver;
	}
	public String getDisplay() {
		return display;
	}
	public void setDisplay(String display) {
		this.display = display;
	}
	public String getResol() {
		return resol;
	}
	public void setResol(String resol) {
		this.resol = resol;
	}
	public String getAccSelector() {
		return accSelector;
	}
	public void setAccSelector(String accSelector) {
		this.accSelector = accSelector;
	}
	public String getCards() {
		return cards;
	}
	public void setCards(String cards) {
		this.cards = cards;
	}
	public String getMainManu() {
		return mainManu;
	}
	public void setMainManu(String mainManu) {
		this.mainManu = mainManu;
	}
	public String getTabs() {
		return tabs;
	}
	public void setTabs(String tabs) {
		this.tabs = tabs;
	}

	@Override
	public String toString() {
		return "TestCaseBean [name=" + name + ", SSN=" + SSN + ", type=" + type
				+ ", req=" + req + ", device=" + device + ", OS=" + OS
				+ ", browser=" + browser + ", ver=" + ver + ", display="
				+ display + ", resol=" + resol + ", accSelector=" + accSelector
				+ ", cards=" + cards + ", mainManu=" + mainManu + ", tabs="
				+ tabs + "]";
	}
	
	
	
	
}
