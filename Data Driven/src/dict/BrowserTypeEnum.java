package dict;
/**
 * Enum to defined browser 
 * @author KPOJASEK
 *
 */
public enum BrowserTypeEnum {
	Firefox_Mozilla, Chrome, Internet_Explorer
}
