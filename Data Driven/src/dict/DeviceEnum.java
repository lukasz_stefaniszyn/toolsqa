package dict;
/**
 * Enum to defined devices.
 * @author KPOJASEK
 *
 */
public enum DeviceEnum {
	Full_screen_desktop, mobile, tablet
}
