package dict;
/**
 * Enum to define operating system type
 * @author KPOJASEK
 *
 */
public enum SystemTypeEnum {
	Windows, iOS, Android
}
