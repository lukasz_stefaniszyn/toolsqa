package searchtest;

import static org.junit.Assert.assertEquals;
import general.BaseTest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;




public class PhantomTest extends BaseTest{

	@Override
	public void load() {
		// TODO Auto-generated method stub
		BaseTest.getDriver().get(BaseTest.getDriver().getCurrentUrl()  + "/");
	}

	@Override
	public void isLoaded() {
		// TODO Auto-generated method stub
		String url = BaseTest.getDriver().getCurrentUrl();
		if (url != "http://www.google.com"){
			try {
				throw new Exception("The wrong page has loaded");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void checkIfPageLoadedCorrectly() {
		// TODO Auto-generated method stub
		
	}
	
	
	@Test
	public void testInputSearchText_OK_1(){
		WebDriver driver = BaseTest.getDriver();
		WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("Cheese!");
        element.submit();
 
        System.out.println("Page title is: " + driver.getTitle());
    }
	
	@Test
	public void testInputSearchText_NOK_1(){
		WebDriver driver = BaseTest.getDriver();
		WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("Have a good day!");
        element.submit();
        System.out.println("Page title is: " + driver.getTitle());
        assertEquals("Planned fail of this TestCase", "Unkown Title", driver.getTitle());
    }




}

	
