package general;


import general.ScreenShotRule;

import java.util.ArrayList;



import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.LoadableComponent;


public abstract class BaseTest extends LoadableComponent<BaseTest>{

	private static WebDriver driver;
	long iStart;
	
	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(WebDriver driver) {
		BaseTest.driver = driver;
	}

	@BeforeClass //This annotation/method is executed before starting this Class
	public static void setUpClass(){

        
    	ArrayList<String> cliArgsCap = new ArrayList<String>();
    	cliArgsCap.add("--web-security=false");
    	cliArgsCap.add("--ssl-protocol=any");
    	cliArgsCap.add("--ignore-ssl-errors=true");
    	DesiredCapabilities cap = DesiredCapabilities.phantomjs();
    	cap.setCapability("takesScreenshot", true);
    	cap.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);
    	cap.setCapability(
    			PhantomJSDriverService.PHANTOMJS_GHOSTDRIVER_CLI_ARGS,
    	        new String[] { "--logLevel=2" });
    	String phantomJsPath = "./lib/phantomjs-2.0.0-windows/bin/phantomjs.exe";
		cap.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,phantomJsPath );
		setDriver(new PhantomJSDriver(cap));
	}
    	
	@AfterClass //This annotation/method is executed after finishing this Class
	public static void tearDownClass(){
		WebDriver driver = BaseTest.getDriver();
		driver.quit();
	
	}
	
	@Before //This annotation/method is executed before each Test execution
	public void setUpTest(){
		WebDriver driver = BaseTest.getDriver();
		driver.get("http://www.google.com");
		
		this.iStart = System.currentTimeMillis(); // start timing
	}
	
	@After  //This annotation/method is executed after each Test execution
	public void tearDownTest(){
        System.out.println("Single Page Time:" + (System.currentTimeMillis() - this.iStart)); // end timing
	}
	
    @Rule  //This annotation/method is executed based on Test execution result. In this scenario when test failed, then we create screenshot
    public ScreenShotRule screenShootRule = new ScreenShotRule(getDriver());
	
	
	
	@Test
	public abstract void checkIfPageLoadedCorrectly();

	@Override
	public abstract void load();

	@Override
	public abstract void isLoaded();

}
