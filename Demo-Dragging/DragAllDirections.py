# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class Draggable(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/tabs/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_draggable(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        for i in range(60):
            try:
                if self.is_element_present(By.LINK_TEXT, "Demo"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        try: self.assertTrue(self.is_element_present(By.LINK_TEXT, "Demo"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Demo", driver.find_element_by_link_text("Demo").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        for i in range(60):
            try:
                if self.is_element_present(By.LINK_TEXT, "Draggable"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        try: self.assertTrue(self.is_element_present(By.LINK_TEXT, "Draggable"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Draggable", driver.find_element_by_link_text("Draggable").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Draggable").click()
        for i in range(60):
            try:
                if self.is_element_present(By.CSS_SELECTOR, "div.inside_contain"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        try: self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.inside_contain"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        for i in range(60):
            try:
                if self.is_element_present(By.ID, "draggable"): break
            except: pass
            time.sleep(1)
        else: self.fail("time out")
        try: self.assertTrue(self.is_element_present(By.ID, "draggable"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [dragAndDrop | id=draggable | +100,0]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [dragAndDrop | id=draggable | 0,+100]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [dragAndDrop | id=draggable | -100,0]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [dragAndDrop | id=draggable | 0,-100]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [dragAndDrop | id=draggable | +200,+200]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [dragAndDrop | id=draggable | +100,-200]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [dragAndDrop | id=draggable | -100,+200]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [dragAndDrop | id=draggable | -200,-200]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionLeft | id=draggable | ]]
        # ERROR: Caught exception [ERROR: Unsupported command [getElementPositionTop | id=draggable | ]]
        driver.close()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
