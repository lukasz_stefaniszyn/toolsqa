*** Settings ***
Suite Setup       Open Browser To Home Page
Suite Teardown    Close Browser
Resource          Resource/ToolQA_TestHome_resource.txt
Resource          ../../Resource/resource.txt

*** Test Cases ***
TC_1_InputData_Registration
    [Tags]    smoke    int
    [Setup]    Open Registration Page URL
    #Sleep is only for presentation purpose
    Sleep    5sec
    Input First Name    Hello
    ##
    Input Last Name    How are You?
    #Sleep is only for presentation purpose
    Push submit
    Sleep    5sec
    Input First Name    HrlloI
    Input
    [Teardown]    Save and Exit Browser

TC_2_InputData_ByCode_Registration
    #Sleep is only for presentation purpose
    Sleep    5sec
    Open Registration Page
    #Sleep is only for presentation purpose
    Sleep    5sec
    Input First Name By Code    Hello by code
    Input Last Name By Code    How are You? By Code
    Push submit By Code
    Sleep    5sec
    [Teardown]    Open Home Page URL
