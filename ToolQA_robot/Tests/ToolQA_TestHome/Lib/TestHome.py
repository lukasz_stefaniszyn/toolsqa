'''
Created on 19.02.2015

@author: LUSTEFAN
'''
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


from robot.utils.asserts import assert_equal
from robot.libraries.BuiltIn import BuiltIn


class TestHome(object):

    __firstname = "name_3_firstname";
    __lastname = "name_3_lastname"; 
    __buttonSubmit = "pie_submit";


    def __get_driver(self):
        se2lib = BuiltIn().get_library_instance('Selenium2Library')
        return se2lib._current_browser()
    driver = property(__get_driver, None, None, None)    


    def __is_element_present(self, how, what):
        """
        Utility method to check presence of an element on page
        :params how: By locator type
        :params what: locator value
        """
        
        driver = self.driver;
        try: 
            driver.find_element(by=how, value=what);
        except NoSuchElementException, e: 
            return False
        return True
    

    def open_registration(self):
        
        driver = self.driver;
        
        self.__is_element_present(By.LINK_TEXT, "Registration");
        driver.find_element(By.LINK_TEXT, "Registration").click();
        assert_equal(driver.find_element(By.CLASS_NAME, "entry-title").text, "Registration");

    def input_text_first_name(self, value):
        driver = self.driver;
        
        firstname = driver.find_element(By.ID, self.__firstname);
        firstname.send_keys(value);

    def input_text_last_name(self, value):
        driver = self.driver;
        
        lastname = driver.find_element(By.ID, self.__lastname);
        lastname.send_keys(value);
        
    def button_submit(self):
        driver = self.driver;
        
        driver.find_element(By.NAME, self.__buttonSubmit).click();
      
    def test(self):
        print("test");
        
    
    
        