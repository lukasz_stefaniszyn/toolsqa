'''
Created on 6 mar 2015

@author: mkonderl
'''
from abc import abstractmethod

class BasePage(object):
    
    def __init__(self, driver):
        self.driver = driver
        self.__validate_page(driver)
        
    @abstractmethod
    def __validate_page(self, driver):
        return
    
class InvalidePageException(Exception):
    pass

