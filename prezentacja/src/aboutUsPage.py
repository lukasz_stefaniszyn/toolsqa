'''
Created on 6 mar 2015

@author: mkonderl
'''
from src.basePage import BasePage
from src.basePage import InvalidePageException


class AboutUsPage(BasePage):
    
    __about_us_title_locator = "entry-title"; #post-156.header.h1
#   __article_locator = "entry-content"; #post-156.div.p
    __article_locator = "div.entry-content p"; #post-156.div.p
    
    def __init__(self,driver):
        super(AboutUsPage,self).__init__(driver);

    def __validate_page(self, driver):
        try:
            driver.find_element_by_class_name(self.__about_us_title_locator);
        except:
            raise InvalidePageException()
    
    @property
    def article(self):
        #return self.driver.find_element_by_class_name(self.driver.__article_locator);
        return self.driver.find_element_by_css_selector(self.__article_locator);
    
    def get_testowa(self):
        return self.driver.find_element_by_css_selector(self.__article_locator);
    testowa = property(get_testowa, None, None, None)
    