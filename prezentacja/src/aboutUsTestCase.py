'''
Created on 6 mar 2015

@author: mkonderl
'''
import unittest
from selenium import webdriver
from aboutUsPage import AboutUsPage
from basePage import BasePage
from baseTestCase import BaseTestCase

class AboutUsTestCase(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self);
        self.driver.get("http://demoqa.com/about-us/");

    def test_AboutUs(self):
        aboutusepage = AboutUsPage(self.driver);
        self.assertEqual(bool(aboutusepage.article.text),True,"Text nie jest true");
    
if __name__ == '__main__':
    unittest.main();
        
    