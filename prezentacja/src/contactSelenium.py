# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
from basePage import BasePage
from baseTestCase import BaseTestCase

class TestContactWprowadzenieDanych(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self);
        self.driver.get("http://demoqa.com/contact/");
    
    def test_contact_wprowadzenie_danych(self):
        driver = self.driver
        #driver.close()
        driver.find_element_by_name("your-name").clear()
        driver.find_element_by_name("your-name").send_keys("Michal konderla")
        driver.find_element_by_name("your-email").clear()
        driver.find_element_by_name("your-email").send_keys("alamakota@wp.pl")
        driver.find_element_by_name("your-subject").clear()
        driver.find_element_by_name("your-subject").send_keys("test kota")
        driver.find_element_by_name("your-message").clear()
        driver.find_element_by_name("your-message").send_keys("to jest test kota aby sprawdzic czy mozna wpisac cos ciekawego")
        driver.implicitly_wait(30);
        driver.find_element_by_css_selector("input.wpcf7-form-control.wpcf7-submit").click()
               
        #driver.get(self.base_url + "")
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        BaseTestCase.tearDown(self);

if __name__ == "__main__":
    unittest.main()