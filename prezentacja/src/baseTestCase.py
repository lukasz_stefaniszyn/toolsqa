from selenium import webdriver
import unittest

class BaseTestCase(unittest.TestCase):
    
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome(r"c:\programs\chromedriver.exe");
        self.driver.maximize_window();
        self.driver.implicitly_wait(30);
    
    def setUp(self):
        pass;
    
    @classmethod
    def tearDownClass(self):        
        self.driver.quit();
        
    def tearDown(self):
        pass;