'''
Created on 6 mar 2015

@author: mkonderl
'''
from basePage import BasePage
from basePage import InvalidePageException
from test.test_xml_etree import xpath_tokenizer

class ContactUsPage(BasePage):
    
    __contact_title_locator = "post-28.header.h1"
    __name_locator = "input[class='wpcf7-form-control wpcf7-text wpcf7-validates-as-required']" #select by css selector
    __e_mail_locator = "input[class='wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email']" 
    __subject_locator = "input[class='wpcf7-form-control wpcf7-text']"
    __message_locator = "textarea[class='wpcf7-form-control wpcf7-textarea']" 
    __send_button_locator = "input[class='wpcf7-form-control wpcf7-submit']"
    __message__validation_locator = "//*[@id='wpcf7-f375-p28-o1']/form/div[2]"
    
    def __init__(self,driver):
        super(ContactUsPage,self).__init__(driver);
    
    @property
    def name_locator(self):
        return self.driver.find_element_by_css_selector(self.__name_locator);
    
    @property
    def e_mail(self):
        return self.driver.find_element_by_css_selector(self.__e_mail_locator);
    
    @property
    def subject(self):
        return self.driver.find_element_by_css_selector(self.__subject_locator);
    
    @property
    def wiadomosc(self):
        return self.driver.find_element_by_css_selector(self.__message_locator);
    
    @property
    def send_button(self):
        return self.driver.find_element_by_css_selector(self.__send_button_locator);
    
    @property  
    def validate_message(self):
        return self.driver.find_element_by_xpath(self.__message__validation_locator); 

    def __validate_page(self, driver):
        try:
            driver.find_element_by_class_name(self.__contact_title_locator);
        except:
            raise InvalidePageException()
        