'''
Created on 6 mar 2015

@author: mkonderl
'''
import unittest, time
from selenium import webdriver
from basePage import BasePage
from baseTestCase import BaseTestCase
from contactUsPage import ContactUsPage


class ContactUsTestCase(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)
        self.driver.get("http://demoqa.com/contact/");
        
    def test_ContactUs_insert_data(self):
        wprowadzDane = ContactUsPage(self.driver);
        wprowadzDane.name_locator.send_keys("Adam Testowy");
        wprowadzDane.e_mail.send_keys("testowy@testowy.com");
        wprowadzDane.subject.send_keys("Test subject automation");
        wprowadzDane.wiadomosc.send_keys("This is the new automated test message");
        wprowadzDane.send_button.click();
        
        time.sleep(5);
        self.assertEquals(wprowadzDane.validate_message.text, "Your message was sent successfully. Thanks.")
    
if __name__ == '__main__':
    unittest.main();