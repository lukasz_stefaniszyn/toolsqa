package tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import pages.MenuPage;

public class MenuTest extends BaseTest{
	
	private static MenuPage menuPage;

	@Before
	public void setUp() {
		menuPage = new MenuPage(driver);
	}
	
	@Override
	@Test
	public void checkIfPageLoadedCorrectly() {
		//Check if on correct page
		assertTrue(menuPage.getActualPageTitle().contains(menuPage.pageTitle()));
		
		//Check if menu on page
		assertNotNull(menuPage.getMenuList());
		
		//Check if menu items text is correct
		verifyAllElementsText(menuPage.getMenuItemsTextList(), menuPage.getMenuList());
		
		//Check if buttons on page
		verifyAllElementsText(menuPage.getButtonsTextList(), menuPage.getTab1Buttons());
	}
	
	@Test
	public void hoverOverButtonsChangeColorForSimpleMenu() {
		//Hover over buttons - buttons change color to orange
		menuPage.getMenuItemHyperlink(0).click();
		
		verifyAllElementsBackgroundColorOnHover(menuPage.getButtonHoverBackgroundColor(), menuPage.getTab1Buttons());
	}
	
	@Test
	public void hoverOverButtonsUnderlineTextForSimpleMenu() {
		//Hover over buttons text - text underlined
		menuPage.getMenuItemHyperlink(0).click();

		verifyAllElementsTextUnderlineOnHover(menuPage.getTab1ButtonsHyperlinks());
	}
	
	@Test
	public void hoverOverButtonsChangeColorForMenuWithSubMenu() {
		//Click on "Menu with Sub Menu"
		menuPage.getMenuItemHyperlink(1).click();
		
		//Check if buttons on page
		assertNotNull(menuPage.getTab2Buttons());
	
		//Hover effect over buttons and submenus
		for (WebElement button : menuPage.getTab2Buttons()) {
			verifyElementBackgroundColorOnHover(menuPage.getButtonHoverBackgroundColor(), button);
		}
	}
	
	@Test
	public void hoverOverButtonsUnderlineTextForMenuWithSubMenu() {
		//Click on "Menu with Sub Menu"
		menuPage.getMenuItemHyperlink(1).click();
		
		//Check if buttons on page
		assertNotNull(menuPage.getTab2Buttons());
		
		//Hover effect over buttons and submenus
		for (WebElement button : menuPage.getTab2ButtonsHyperlinks()) {
			verifyElementTextUnderlineOnHover(button); //it moves mouse to this button at the same time
		}
	}
	
	@Test
	public void hoverOverButtonsChangeColorForSubMenus() {
		//Click on "Menu with Sub Menu"
		menuPage.getMenuItemHyperlink(1).click();
		
		//Check if buttons on page
		assertNotNull(menuPage.getTab2Buttons());
		
		//Hover effect over buttons and submenus
		for (WebElement button : menuPage.getTab2Buttons()) {
			action.moveToElement(button).build().perform();
			verifyAllElementsBackgroundColorOnHover(menuPage.getButtonHoverBackgroundColor(), menuPage.getButtonSubmenu(button));
		}
	}
	
	@Test
	public void hoverOverButtonsUnderlineTextForSubMenus() {
		//Click on "Menu with Sub Menu"
		menuPage.getMenuItemHyperlink(1).click();
		
		//Check if buttons on page
		assertNotNull(menuPage.getTab2Buttons());
		
		//Hover effect over buttons and submenus
		for (WebElement button : menuPage.getTab2Buttons()) {
			action.moveToElement(button).build().perform();
			verifyAllElementsTextUnderlineOnHover(menuPage.getButtonSubmenuHyperlinks(button));
		}
	}
	
	@Test
	public void menuItemIsSelectedAfterClick() {
		menuPage.getMenuItemHyperlink(1).click();
		verifyMenuItemSelected(menuPage.getMenuItem(1));
		
		menuPage.getMenuItemHyperlink(0).click();
		verifyMenuItemSelected(menuPage.getMenuItem(0));
	}

}

