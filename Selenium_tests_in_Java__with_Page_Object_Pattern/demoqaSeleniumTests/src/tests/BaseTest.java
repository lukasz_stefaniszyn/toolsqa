package tests;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public abstract class BaseTest {

	protected static FirefoxDriver driver;
	protected static Actions action;
	
	@BeforeClass
	public static void setUpClass() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		action = new Actions(driver);
	}
	
	@AfterClass
	public static void tearDownClass() {
		driver.quit();
	}
	
	@Test
	public abstract void checkIfPageLoadedCorrectly();
	
	public void verifyElementText( String text, WebElement webElement) {
		assertEquals(text, webElement.getText());
	}
	
	public void verifyAllElementsText(String[] textList, List<WebElement> webElementsList) {
		assertEquals(textList.length, webElementsList.size());
		for (int iterator=0; iterator < textList.length; iterator++) {
			verifyElementText(textList[iterator], webElementsList.get(iterator));
		}
	}
	
	public void verifyElementBackgroundColor(String color, WebElement element) {
		//check if buttons change color 
		assertEquals(color, element.getCssValue("background-color"));
	}
	
	public void verifyElementBackgroundColorOnHover(String color, WebElement element) {
		action.moveToElement(element).build().perform();
		//check if buttons change color 
		verifyElementBackgroundColor(color, element);
	}
	
	public void verifyAllElementsBackgroundColorOnHover(String color, List<WebElement> webElementsList) {
		for (WebElement button : webElementsList) {
			verifyElementBackgroundColorOnHover(color, button);
		}
	}
	
	public void verifyAllElementsTextUnderlineOnHover(List<WebElement> webElementsList) {
		for (WebElement button : webElementsList) {
			verifyElementTextUnderlineOnHover(button);
		}
	}
	
	public void verifyElementTextUnderlineOnHover(WebElement button) {
		action.moveToElement(button).build().perform();
		//check if text is underlined
		assertEquals("underline", button.getCssValue("text-decoration"));
	}

	public void verifyElementReadonly (WebElement element) {
		assertEquals("true", element.getAttribute("readonly"));
	}
	
	public void verifyElementFontSize (String size, WebElement element) {
		assertEquals(size, element.getCssValue("font-size"));
	}
	
	public void verifyMenuItemSelected(WebElement menuItem) {
		assertEquals("true", menuItem.getAttribute("aria-selected"));
	}
}
