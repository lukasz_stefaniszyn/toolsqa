package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import pages.SliderPage;


public class SliderTest extends BaseTest {

	private static SliderPage sliderPage;

	@Before
	public void setUp() {
		sliderPage = new SliderPage(driver);
	}
	
	@Override
	@Test
	public void checkIfPageLoadedCorrectly() {
		//Check if on correct page
		assertTrue(sliderPage.getActualPageTitle().contains(sliderPage.pageTitle()));
		
		//Check if menu on page
		assertNotNull(sliderPage.getMenuItem());
		
		//Check if menu items text is correct
		verifyElementText( sliderPage.getMenuItemText(), sliderPage.getMenuItem());
		
		//Check if slider on page
		assertNotNull(sliderPage.getSlider());
		assertNotNull(sliderPage.getSliderHandler());
		
		//Check if amount on page
		assertNotNull(sliderPage.getAmountLabel());
		verifyElementText(sliderPage.getAmountLabelText(), sliderPage.getAmountLabel());
		assertNotNull(sliderPage.getAmount());
		
		//Check if amount value readonly
		verifyElementReadonly(sliderPage.getAmount());
	}
	
	@Test
	public void sliderIsWhiteWhenSelected() {
		action.clickAndHold(sliderPage.getSliderHandler()).build().perform();
		verifyElementBackgroundColor(sliderPage.getRgbWhite(), sliderPage.getSliderHandler());
		action.release(sliderPage.getSliderHandler()).build().perform();
	}
	
	@Test
	public void valueChangesWhenSliderMovedLeft() {
		String valueBefore = sliderPage.getAmountValue();
		//click and move right
		action.clickAndHold(sliderPage.getSliderHandler()).moveByOffset(-10000, 0).build().perform();
		String valueAfter = sliderPage.getAmountValue();
		action.release(sliderPage.getSliderHandler()).build().perform();
		
		//check if value has changed
		assertNotEquals(valueBefore, valueAfter);
	}
	
	@Test
	public void valueChangesWhenSliderMovedRight() {
		String valueBefore = sliderPage.getAmountValue();
		//click and move right
		action.clickAndHold(sliderPage.getSliderHandler()).moveByOffset(10000, 0).build().perform();
		String valueAfter = sliderPage.getAmountValue();
		action.release(sliderPage.getSliderHandler()).build().perform();
		
		//check if value has changed
		assertNotEquals(valueBefore, valueAfter);
	}
	
	@Test
	public void sliderMovesAndValueChangesWhenClickOnSlider() {
		String valueBefore = sliderPage.getAmountValue();
		sliderPage.getSlider().click();
		String valueAfter = sliderPage.getAmountValue();
		assertNotEquals(valueBefore, valueAfter);
	}
	
	@Test
	public void valueInputCannotBeModified() {
		String valueBefore = sliderPage.getAmountValue();
		sliderPage.getAmount().sendKeys("aB133");
		String valueAfter = sliderPage.getAmountValue();
		assertEquals(valueBefore, valueAfter);
	}

}

