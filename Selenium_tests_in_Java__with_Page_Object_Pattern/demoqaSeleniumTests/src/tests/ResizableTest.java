package tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import pages.ResizablePage;

public class ResizableTest extends BaseTest {
	
	private static ResizablePage resizablePage;

	@Before
	public void setUp() {
		resizablePage = new ResizablePage(driver);
	}
	
	@Override
	@Test
	public void checkIfPageLoadedCorrectly() {
		//Check if on correct page
		assertTrue(resizablePage.getActualPageTitle().contains(resizablePage.pageTitle()));
		
		//Check if header text is correct
		verifyElementText(resizablePage.getHeaderText(), resizablePage.getHeader());
		verifyElementFontSize(resizablePage.getHeaderFontSize(), resizablePage.getHeader());
		
		//Check if 5 tabs in menu and if correct text
		verifyAllElementsText(resizablePage.getMenuItemsNames(), resizablePage.getMenuItems());
		
		//Check if "Default functionality" is active
		verifyElementBackgroundColor(resizablePage.getActiveMenuItemBackgroundColor(), resizablePage.getMenuItem(0));
		verifyMenuItemSelected(resizablePage.getMenuItem(0));

		//Check if resizable shape on page
		verifyElementText(resizablePage.getResizableShapeText(), resizablePage.getResizableShape());
	}

	@Test
	public void resizableShapeCanBeStretchedHorizontalyByEdge() {
		//make shape wider and extend borders
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableVerticalEdgeHandle(), 2000, 0));
		
		//make shape thiner
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableVerticalEdgeHandle(), -2000, 0));
	}
	
	@Test
	public void resizableShapeCanBeStretchedVerticalyByEdge() {
		//make shape taller and extend borders
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableHorizontalEdgeHandle(), 0, 2000));
		
		//make shape lower
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableHorizontalEdgeHandle(), 0, -2000));
	}
	
	@Test
	public void resizableShapeCanBeStretchedHorizontalyByCorner() {
		//make shape wider
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableCornerHandle(), 200, 0));
		
		//make shape thiner
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableCornerHandle(), -200, 0));
	}
	
	@Test
	public void resizableShapeCanBeStretchedVerticalyByCorner() {
		//make shape taller
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableCornerHandle(), 0, 100));
		
		//make shape lower
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableCornerHandle(), 0, -100));
	}
	
	@Test
	public void resizableShapeCanBeStretchedVerticalyAndHorizontalyAtTheSameTime() {
		//make shape bigger
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableCornerHandle(), 200, 100));
		
		//make shape smaller
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableCornerHandle(), -200, -100));
		
		//make shape wider and lower
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableCornerHandle(), 200, -100));
		
		//make shape thiner and taller
		assertTrue(elementDimensionChangedAfterResizeBy(resizablePage.getResizableShape(), resizablePage.getResizableCornerHandle(), -200, 100));
	}

	private boolean elementDimensionChangedAfterResizeBy(WebElement resizableShape, WebElement handle, int x, int y) {
		String widthBefore;
		String widthAfter;
		String heightBefore;
		String heightAfter;
		if (x == 0) {
			heightBefore = resizableShape.getCssValue("height");
			action.dragAndDropBy(handle, x, y).build().perform();
			heightAfter = resizableShape.getCssValue("height");
			return (heightBefore != heightAfter);
		} else if (y == 0) {
			widthBefore = resizableShape.getCssValue("width");
			action.dragAndDropBy(handle, x, y).build().perform();
			widthAfter = resizableShape.getCssValue("width");
			return (widthBefore != widthAfter);
		} else {
			heightBefore = resizableShape.getCssValue("height");
			widthBefore = resizableShape.getCssValue("width");
			action.dragAndDropBy(handle, x, y).build().perform();
			heightAfter = resizableShape.getCssValue("height");
			widthAfter = resizableShape.getCssValue("width");
			return ( (widthBefore != widthAfter) && (heightBefore != heightAfter) );
		}
	}
}
