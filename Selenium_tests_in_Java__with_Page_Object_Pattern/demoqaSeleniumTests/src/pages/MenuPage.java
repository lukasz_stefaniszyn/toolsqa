package pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MenuPage extends DemoqaPage {
	
	private static final String subUrl = "menu";
	private static final String title = "Menu";
	private static final String menuItemsSelector = "div[id='tabs'] > ul > li";
	private static final String menuItemsHyperlinkSelector = "div[id='tabs'] > ul > li > a";
	private static final String menuItemsTextSelector = "div[id='tabs'] > ul > li > a";
	private static final String tab1ButtonsSelector = "div[id='tabs-1'] > div > div > ul > li";
	private static final String tab1ButtonsHyperlinkSelector = "div[id='tabs-1'] > div > div > ul > li > a";
	private static final String tab2ButtonsSelector = "div[id='tabs-2'] > div > div > ul > li";
	private static final String tab2ButtonsHyperlinkSelector = "div[id='tabs-2'] > div > div > ul > li > a";
	private static final String[] buttonsTextList = {"Home", "About", "Contact", "FAQ", "News"};
	private static final String[] menuItemsTextList = {"Simple Menu", "Menu With Sub Menu"};
	private static final String buttonHoverBackgroundColor = "rgba(255, 153, 0, 1)";
	
	public MenuPage(FirefoxDriver driver) {
		super(subUrl, driver);
	}
	
	@Override
	public String pageTitle() {
		return title;
	}
	
	public String getMenuItemText (int item) {
		List<WebElement> itemTextList = driver.findElements(By.cssSelector(menuItemsTextSelector));
		return itemTextList.get(item).getText();
	}
	
	public WebElement getMenuItem (int item) {
		List<WebElement> itemsList = driver.findElements(By.cssSelector(menuItemsSelector));
		return itemsList.get(item);
	}
	
	public WebElement getMenuItemHyperlink (int item) {
		List<WebElement> itemsList = driver.findElements(By.cssSelector(menuItemsHyperlinkSelector));
		return itemsList.get(item);
	}
	
	public List<WebElement> getMenuList () {
		return driver.findElements(By.cssSelector(menuItemsSelector));
	}
	
	public List<WebElement> getTab1Buttons () {
		return driver.findElements(By.cssSelector(tab1ButtonsSelector));
	}
	
	public List<WebElement> getTab1ButtonsHyperlinks () {
		return driver.findElements(By.cssSelector(tab1ButtonsHyperlinkSelector));
	}
	
	public List<WebElement> getTab2Buttons () {
		return driver.findElements(By.cssSelector(tab2ButtonsSelector));
	}
	
	public List<WebElement> getTab2ButtonsHyperlinks () {
		return driver.findElements(By.cssSelector(tab2ButtonsHyperlinkSelector));
	}
	
	public List<WebElement> getButtonSubmenu (WebElement button) {
		return button.findElements(By.cssSelector("li"));
	}
	
	public List<WebElement> getButtonSubmenuHyperlinks (WebElement button) {
		return button.findElements(By.cssSelector("li > a"));
	}
	
	public String[] getButtonsTextList() {
		return buttonsTextList;
	}
	
	public String[] getMenuItemsTextList() {
		return menuItemsTextList;
	}
	
	public String getButtonHoverBackgroundColor() {
		return buttonHoverBackgroundColor;
	}

}
