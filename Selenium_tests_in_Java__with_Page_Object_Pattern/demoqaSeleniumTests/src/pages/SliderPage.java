package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SliderPage extends DemoqaPage {
	
	private static final String subUrl = "slider";
	private static final String title = "Slider";
	private static final String sliderSelector = "div[id='slider-range-max']";
	private static final String amountValueSelector = "input[id='amount1']";
	private static final String amountLabelSelector = "label[for='amount1']";
	private static final String menuItemSelector = "a[id='ui-id-1']";
	private static final String sliderHandlerSelector = "div[id='slider-range-max'] > a";
	private static final String amountLabelText = "Minimum number of bedrooms:";
	private static final String menuItemText = "Range slider";
	
	public SliderPage(FirefoxDriver driver) {
		super(subUrl, driver);
	}

	@Override
	public String pageTitle() {
		return title;
	}
	
	public WebElement getSlider() {
		return driver.findElement(By.cssSelector(sliderSelector));
	}

	
	public WebElement getAmount() {
		return driver.findElement(By.cssSelector(amountValueSelector));
	}
	
	public String getAmountValue() {
		return driver.findElement(By.cssSelector(amountValueSelector)).getAttribute("value");
	}
	
	public WebElement getAmountLabel() {
		return driver.findElement(By.cssSelector(amountLabelSelector));
	}

	public WebElement getMenuItem() {
		return driver.findElement(By.cssSelector(menuItemSelector));
	}
	
	public WebElement getSliderHandler() {
		return driver.findElement(By.cssSelector(sliderHandlerSelector));
	}
	
	public String getAmountLabelText() {
		return amountLabelText;
	}
	
	public String getMenuItemText() {
		return menuItemText;
	}

}
