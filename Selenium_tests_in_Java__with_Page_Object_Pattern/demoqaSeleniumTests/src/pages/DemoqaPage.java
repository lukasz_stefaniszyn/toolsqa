package pages;

import org.openqa.selenium.firefox.FirefoxDriver;

abstract public class DemoqaPage {
	
	private  final String url = "http://demoqa.com/"; 
	protected FirefoxDriver driver;
	private final String rgbWhite = "rgba(255, 255, 255, 1)";
	private final String activeMenuItemBackgroundColor = rgbWhite;
		
	DemoqaPage(String subUrl, FirefoxDriver driver) {
		this.driver = driver;
		driver.get(url+subUrl);
	}
	
	public String getActiveMenuItemBackgroundColor() {
		return activeMenuItemBackgroundColor;
	}

	public String getRgbWhite() {
		return rgbWhite;
	}
	
	public String getActualPageTitle() {
		return driver.getTitle();
	}
	
	public abstract String pageTitle();
}
