package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ResizablePage extends DemoqaPage {
	
	private static final String subUrl = "resizable";
	private static final String title = "Resizable";
	
	private static final String headerSelector = "div[class='entry-content'] > h2";
	private static final String headerText = "Change the size of an element using the mouse.";
	private static final String headerFontSize = "30px";
	
	private static final String menuItemsSelector = "div[id='tabs'] > ul > li";
	private static final String[] menuItemsNames = {"Default functionality", "Animate", "Constrain resize area", "Helper", "Max/Min size"}; 

	private static final String resizableShapeSelector = "div[id='resizable']";
	private static final String resizableCornerHandleSelector = "div[id='resizable'] > div:nth-child(4)";
	private static final String resizableHorizontalEdgeHandleSelector = "div[id='resizable'] > div:nth-child(3)";
	private static final String resizableVerticalEdgeHandleSelector = "div[id='resizable'] > div:nth-child(2)";
	private static final String resizableShapeText = "Resizable";
	
	public ResizablePage(FirefoxDriver driver) {
		super(subUrl, driver);
	}

	@Override
	public String pageTitle() {
		return title;
	}
	
	public WebElement getHeader() {
		return driver.findElement(By.cssSelector(headerSelector));
	}

	public String getHeaderText() {
		return headerText;
	}
	
	public String getHeaderFontSize() {
		return headerFontSize;
	}

	public List<WebElement> getMenuItems() {
		return driver.findElements(By.cssSelector(menuItemsSelector));
	}
	
	public WebElement getMenuItem(int index) {
		return driver.findElements(By.cssSelector(menuItemsSelector)).get(index);
	}
	
	public String[] getMenuItemsNames() {
		return menuItemsNames;
	}
	
	public WebElement getResizableShape() {
		return driver.findElement(By.cssSelector(resizableShapeSelector));
	}
	
	public WebElement getResizableCornerHandle() {
		return driver.findElement(By.cssSelector(resizableCornerHandleSelector));
	}
	
	
	public WebElement getResizableHorizontalEdgeHandle() {
		return driver.findElement(By.cssSelector(resizableHorizontalEdgeHandleSelector));
	}
	
	public WebElement getResizableVerticalEdgeHandle() {
		return driver.findElement(By.cssSelector(resizableVerticalEdgeHandleSelector));
	}
	
	public String getResizableShapeText() {
		return resizableShapeText;
	}
}