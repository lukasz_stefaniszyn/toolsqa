# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class AboutUs3StylingBold(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/about-us/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_about_us3_styling_bold(self):
        driver = self.driver
        driver.get(self.base_url + "")
        try: self.assertEqual("Lorem Ipsum", driver.find_element_by_css_selector("strong").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.close()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
