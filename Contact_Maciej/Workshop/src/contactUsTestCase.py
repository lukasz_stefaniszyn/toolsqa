'''
Created on 9 mar 2015

@author: MACZERNI
'''
import unittest, time
from contactUsPage import ContactUsPage
from BaseTestCase import BaseTestCase
from selenium import webdriver


class ContactTestCase(BaseTestCase):

    def setUp(self):
        # http://demoqa.com/contact/
        BaseTestCase.setUp(self);
        self.driver.get("http://demoqa.com/contact/");
        
    def testContact(self):
        contactpage = ContactUsPage(self.driver)
        contactpage.name.send_keys("123")
        contactpage.email.send_keys("456")
        contactpage.subject.send_keys("testowy temat")
        contactpage.message.send_keys("taki tam message")
        contactpage.click_send.click()
        time.sleep(5);
        #self.assertEquals(contactpage.validate_message(), "error", "nie pyklo")
        # if contactpage.validate_message.text
        #    return "True"
        #else:
        #   return "False"
        self.assertEquals(contactpage.validate_message.text, "Validation errors occurred. Please confirm the fields and submit it again.")

if __name__ == '__main__':
    unittest.main()