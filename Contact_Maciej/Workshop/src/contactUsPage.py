'''
Created on 9 mar 2015

@author: MACZERNI
'''
from src.BasePage import BasePage
from BasePage import InvalidPageException

class ContactUsPage(BasePage):
    
    __name_locator = "input[class='wpcf7-form-control wpcf7-text wpcf7-validates-as-required']" #select by css selector 

    #__e_mail_locator = "span[class='wpcf7-form-control your-email']"
    __email_locator = \
    "input[class='wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email']"

    #__subject_locator = "span[class='wpcf7-form-control your-subject']"
    __subject_locator = "input[class='wpcf7-form-control wpcf7-text']"
    #__message_locator = "span[class='wpcf7-form-control your-message']"
    __message_locator = "textarea[class='wpcf7-form-control wpcf7-textarea']"
    #__click_send_locator = "span[class='wpcf7-form-control your-message']"
    __click_send_locator = "input[class='wpcf7-form-control wpcf7-submit']"
    __validation_message_locator = '//*[@id="wpcf7-f375-p28-o1"]/form/div[2]' #xpath


    def __init__(self, driver):
        super(ContactUsPage, self).__init__(driver),
        
    @property
    def name(self):
        return self.driver.find_element_by_css_selector(self.__name_locator)
    
    @property
    def email(self):
        return self.driver.find_element_by_css_selector(self.__email_locator)
    
    @property
    def subject(self):
        return self.driver.find_element_by_css_selector(self.__subject_locator)
    
    @property
    def message(self):
        return self.driver.find_element_by_css_selector(self.__message_locator)
    
    @property
    def click_send(self):
        return self.driver.find_element_by_css_selector(self.__click_send_locator)
    
    @property
    def validate_message(self):
        return self.driver.find_element_by_xpath(self.__validation_message_locator)
        
    def __validate_page_by_name(self, driver):
        try: 
            driver.find_element_by_css_selector(self.__name_locator)
        except:
            raise InvalidPageException("Brak pola name")
        
    #def validate_message(self): #xpath
    #   if "errors" in self.driver.find_element_by_xpath(self.__validation_message_locator).text:
    #        return "False"
    #    else:
    #        return "True"
    
    def __validate_page(self, driver):
        try:
            driver.find_element_by_class_name(self.__contact_title_locator);
        except:
            raise InvalidPageException()    