'''
Created on 6 mar 2015

@author: MACZERNI
'''
import unittest
from AboutUsPage import AboutUsPage
from BaseTestCase import BaseTestCase
from selenium import webdriver


class AboutUsTestCase(BaseTestCase):
    
    def setUp(self):
        # http://demoqa.com/about-us/
        BaseTestCase.setUp(self);
        self.driver.get("http://demoqa.com/about-us/");
        
    def testAboutUs(self):
        aboutuspage = AboutUsPage(self.driver);
        self.assertEquals(bool(aboutuspage.article.text), True, "CHUJOWO");
        
if __name__ == '__main__':
    unittest.main()