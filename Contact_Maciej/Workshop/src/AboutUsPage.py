'''
Created on 6 mar 2015

@author: MACZERNI
'''

from src.BasePage import BasePage
from BasePage import InvalidPageException

class AboutUsPage(BasePage):
    '''klasa about us page'''
    
    __about_us_title_locator = \
        'post-156.header.h1'
    __article_locator = \
        'div.entry-content p'
    
    '''lokatory elementow w css'''
    
    def __init__(self, driver):
        super(AboutUsPage, self).__init__(driver),
        '''super wywoluje definicje z nadrzednej'''
        
        
    @property
    def article(self):
        return self.driver.find_element_by_css_selector(self.__article_locator)
    '''zdefiniowanie atrybutu klasy przy uzyciu metody korzystajacej z dekoratora property 
    i lokatora zdefiniowanego wczesniej'''
    
    def __validate_page(self, driver):
        try: 
            driver.find_element_by_css_selector(self.__about_us_title_locator);
        except:
            raise InvalidPageException("About Us Page Not Loaded")
        '''znajdz po css korzystajac ze zdefiniowanej metody a jak nie znajdzie wyjeb exception'''
       
       
            
        