'''
Created on 6 mar 2015

@author: MACZERNI
'''

from abc import abstractmethod

class BasePage(object):
    '''
    classdocs
    '''

    def __init__(self, driver):
        self.driver=driver
        self.__validate_page(driver)


    @abstractmethod    
    def __validate_page(self, driver):
        pass
class InvalidPageException(Exception):
    pass
