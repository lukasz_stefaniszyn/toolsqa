'''
Created on 6 mar 2015

@author: MACZERNI
'''
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
'''import bezposredni unittest i import webdrivera z biblioteki selenium'''

class BaseTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome();
        cls.driver.maximize_window();
        cls.driver.implicitly_wait(30);  
    
    def setup(self):
        pass;
    
    def tearDown(self):
        pass;
    
    @classmethod
    def TearDownClass(cls):
        self.driver.quit();
        
        