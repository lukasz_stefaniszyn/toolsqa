'''
Created on 02.03.2015

@author: SKRUSZKO
'''

from BasePage import *

class ServicesPage(BasePage):
    
    __services_title_locator = \
        'post-114.header.h1'
        
    __article_locator = \
        'div.entry-content p'
        
    def __init__(self, driver):
        super(ServicesPage, self).__init__(driver)
        
    @property
    def article(self):
        return self.driver.find_element_by_css_selector(self.__article_locator)
        
    def __validate_page(self, driver):
        try:
            driver.find_element_by_css_selector(self.__services_title_locator)
        except:
            raise InvalidPageException("Services Page not loaded")