'''
Created on 03.03.2015

@author: SKRUSZKO
'''
import unittest
from ServicesPage import ServicesPage
from BaseTestCase import BaseTestCase

class ServicesTestCase(BaseTestCase):
    
    def setUp(self):
        BaseTestCase.setUp(self)
        self.driver.get('http://demoqa.com/contact/')
        
    def test_services(self):
        servicespage = ServicesPage(self.driver)
        self.assertEquals(bool(servicespage.article.text), True, "Article content is empty.")
        
if __name__ == '__main__':
    unittest.main(verbosity=2)