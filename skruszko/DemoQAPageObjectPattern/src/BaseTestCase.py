'''
Created on 02.03.2015

@author: SKRUSZKO
'''
import unittest
from selenium import webdriver

class BaseTestCase(unittest.TestCase):
    
    
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.implicitly_wait(30)
        cls.driver.maximize_window()
    
    def setUp(self):
        pass
        
    def tearDown(self):
        pass
        
        
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()    