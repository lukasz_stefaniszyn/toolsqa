'''
Created on 02.03.2015

@author: SKRUSZKO
'''
from BasePage import *
from selenium.common.exceptions import NoSuchElementException

class ContactPage(BasePage):
    
    __contact_title_locator = \
        'post-28.header.h1'
        
    __name_locator = \
        "input[class='wpcf7-form-control wpcf7-text wpcf7-validates-as-required']"
        
    __e_mail_locator = \
        "input[class='wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email']"
        
    __subject_locator = \
        "input[class='wpcf7-form-control wpcf7-text']"
        
    __message_locator = \
        "textarea[class='wpcf7-form-control wpcf7-textarea']"
        
        
    __send_button_locator = \
        "input[class='wpcf7-form-control wpcf7-submit']"
        
    __message_validation_locator = \
        '//*[@id="wpcf7-f375-p28-o1"]/form/div[2]'
    
    def __init__(self, driver):
        super(ContactPage, self).__init__(driver)
        
    @property
    def name(self):
        return self.driver.find_element_by_css_selector(self.__name_locator)
         
        #=======================================================================
        # self.driver.find_element_by_name()
        # self.driver.find_element_by_id()
        # return self.driver.find_element_by_class_name(r"wpcf7-form-control wpcf7-text wpcf7-validates-as-required")
        # css
        # xpath
        #=======================================================================
         
    @property
    def e_mail(self):
        return self.driver.find_element_by_css_selector(self.__e_mail_locator)
    
    @property
    def subject(self):
        return self.driver.find_element_by_css_selector(self.__subject_locator)
    
    @property
    def message(self):
        return self.driver.find_element_by_css_selector(self.__message_locator)
    
    @property
    def send_button(self):
        return self.driver.find_element_by_css_selector(self.__send_button_locator)
        
    def __validate_page(self, driver):
        try:
            driver.find_element_by_css_selector(self.__contact_title_locator)
        except:
            raise InvalidPageException("Contact Page not loaded")
        
    def validate_message(self):
        if "successfully" in self.driver.find_element_by_xpath(self.__message_validation_locator).text:
            return "True"
        else:
            return "False"
        
        