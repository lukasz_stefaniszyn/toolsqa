'''
Created on 03.03.2015

@author: SKRUSZKO
'''
import unittest, time, csv
from ContactPage import ContactPage
from BaseTestCase import BaseTestCase
from ddt import ddt, data, unpack

def get_data(file_name):
    rows = []
    file = open(file_name, "rb")
    reader = csv.reader(file)
    next(reader, None)
    for row in reader:
        rows.append(row)
    return rows

@ddt
class SendMessageTestCase(BaseTestCase):
    
    def setUp(self):
        BaseTestCase.setUp(self)
        self.driver.get('http://demoqa.com/contact/')
        
    @data(*get_data("testdata.csv"))
    @unpack
    def test_send_message(self, name_string, e_mail_string, subject_string, message_string, expected_result):
        contactpage = ContactPage(self.driver)
        contactpage.name.send_keys(name_string.strip())
        contactpage.e_mail.send_keys(e_mail_string.strip())
        contactpage.subject.send_keys(subject_string.strip())
        contactpage.message.send_keys(message_string.strip())
        contactpage.send_button.click()
        time.sleep(1)
        self.assertEquals(contactpage.validate_message(), expected_result.strip(), "Expected result was not achieved.")
        
        
if __name__ == '__main__':
    unittest.main(verbosity=2)