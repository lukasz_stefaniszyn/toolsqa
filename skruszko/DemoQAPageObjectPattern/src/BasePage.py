'''
Created on 02.03.2015

@author: SKRUSZKO
'''
from abc import abstractmethod

class BasePage(object):
    
    def __init__(self, driver):
        self.__validate_page(driver)
        self.driver = driver
        
    @abstractmethod
    def __validate_page(self, driver):
        return
    
class InvalidPageException(Exception):
    pass