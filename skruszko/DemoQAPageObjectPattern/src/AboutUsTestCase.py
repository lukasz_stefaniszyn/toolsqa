'''
Created on 03.03.2015

@author: SKRUSZKO
'''
import unittest
from AboutUsPage import AboutUsPage
from BaseTestCase import BaseTestCase

class AboutUsTestCase(BaseTestCase):
    
    def setUp(self):
        BaseTestCase.setUp(self)
        self.driver.get('http://demoqa.com/about-us/')
        
    def test_about_us(self):
        aboutuspage = AboutUsPage(self.driver)
        self.assertEquals(bool(aboutuspage.article.text), True, "Article content is empty.")
        
if __name__ == '__main__':
    unittest.main(verbosity=2)