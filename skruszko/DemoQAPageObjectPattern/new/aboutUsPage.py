'''
Created on 06.03.2015

@author: SKRUSZKO
'''
from basePage import BasePage
from selenium.common.exceptions import NoSuchElementException
from new.basePage import InvalidPageException

class AboutUsPage(BasePage):
    
    
    
    __about_us_title_locator = 'post-156.header.h1'
        
    __article_locator = 'div.entry-content p'
    
    
    def __init__(self, driver):
        super(AboutUsPage, self).__init__(driver);
        
    
    def __validate_page(self, driver):
        try: 
            driver.find_element_by_css_selector(
                self.__about_us_title_locator);
        except:
            raise InvalidPageException();
    
    @property    
    def article(self):
        return self.driver.find_element_by_css_selector(
                    self.__article_locator);
                    
                    
    
                
        
        
        
    