'''
Created on 06.03.2015

@author: SKRUSZKO
'''
from selenium import webdriver
import unittest


class BaseTestCase(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome();
        cls.driver.maximize_window();
        cls.driver.implicitly_wait(30);
        
    
    def setup(self):
        pass;
    
    def tearDown(self):
        pass;
    
    
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit();
        
    