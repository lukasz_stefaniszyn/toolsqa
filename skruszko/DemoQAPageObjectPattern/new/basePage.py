'''
Created on 06.03.2015

@author: SKRUSZKO
'''

from abc import abstractmethod

class BasePage(object):
    '''
    classdocs
    '''

    
    def __init__(self, driver):
        self.driver = driver;
        self.__validate_page(driver);
        
        
    @abstractmethod
    def __validate_page(self, driver):
        return
        

class InvalidPageException(Exception):
    pass

