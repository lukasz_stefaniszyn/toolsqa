# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class FirstName(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/registration/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_first_name(self):
        driver = self.driver
        driver.get(self.base_url + "/registration/")
        driver.find_element_by_name("pie_submit").click()
        try: self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "#post-49 > div > p > strong:nth-child(7)"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("#post-49 > div > p > strong:nth-child(7)").text, r"^exact:[\s\S]* Error: Username is required$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        
#    css_selector    "#post-49 > div > p > strong:nth-child(1)" - username invalid
#    css_selector    "#post-49 > div > p > strong:nth-child(3)" - first name
#    css_selector    "#post-49 > div > p > strong:nth-child(5)" - hobby
#    css_selector    "#post-49 > div > p > strong:nth-child(7)" - username required
#    css_selector    "#post-49 > div > p > strong:nth-child(9)" - e-mail invalid
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
