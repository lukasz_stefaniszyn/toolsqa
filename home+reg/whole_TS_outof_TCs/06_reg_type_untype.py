# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class TypeUntypeSelectUnselect(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_type_untype_select_unselect(self):
        driver = self.driver
        driver.get(self.base_url + "/registration/")
        driver.find_element_by_id("name_3_firstname").clear()
        driver.find_element_by_id("name_3_firstname").send_keys("abcABC")
        try: self.assertEqual("abcABC", driver.find_element_by_id("name_3_firstname").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("name_3_firstname").clear()
        driver.find_element_by_id("name_3_firstname").send_keys("")
        try: self.assertEqual("", driver.find_element_by_id("name_3_firstname").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("name_3_lastname").clear()
        driver.find_element_by_id("name_3_lastname").send_keys("XYZxyz")
        try: self.assertEqual("XYZxyz", driver.find_element_by_id("name_3_lastname").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("name_3_lastname").clear()
        driver.find_element_by_id("name_3_lastname").send_keys("")
        try: self.assertEqual("", driver.find_element_by_id("name_3_lastname").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("off", driver.find_element_by_name("radio_4[]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_name("radio_4[]").click()
        try: self.assertEqual("on", driver.find_element_by_name("radio_4[]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("off", driver.find_element_by_xpath("(//input[@name='radio_4[]'])[2]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("(//input[@name='radio_4[]'])[2]").click()
        try: self.assertEqual("on", driver.find_element_by_xpath("(//input[@name='radio_4[]'])[2]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("off", driver.find_element_by_xpath("(//input[@name='radio_4[]'])[3]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("(//input[@name='radio_4[]'])[3]").click()
        try: self.assertEqual("on", driver.find_element_by_xpath("(//input[@name='radio_4[]'])[3]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("off", driver.find_element_by_name("checkbox_5[]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_name("checkbox_5[]").click()
        try: self.assertEqual("on", driver.find_element_by_name("checkbox_5[]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_name("checkbox_5[]").click()
        try: self.assertEqual("off", driver.find_element_by_name("checkbox_5[]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("off", driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[2]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[2]").click()
        try: self.assertEqual("on", driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[2]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[2]").click()
        try: self.assertEqual("off", driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[2]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("off", driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[3]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[3]").click()
        try: self.assertEqual("on", driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[3]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[3]").click()
        try: self.assertEqual("off", driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[3]").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        Select(driver.find_element_by_xpath("//select")).select_by_visible_text("Benin")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedValue | id=dropdown_7 | ]]
        Select(driver.find_element_by_xpath("//select")).select_by_visible_text("Poland")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedValue | id=dropdown_7 | ]]
        try: self.assertTrue(self.is_element_present(By.ID, "mm_date_8"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        Select(driver.find_element_by_id("mm_date_8")).select_by_visible_text("10")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedIndex | id=mm_date_8 | ]]
        Select(driver.find_element_by_id("mm_date_8")).select_by_visible_text("5")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedIndex | id=mm_date_8 | ]]
        Select(driver.find_element_by_id("mm_date_8")).select_by_visible_text("Month")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedLabel | id=mm_date_8 | ]]
        try: self.assertTrue(self.is_element_present(By.ID, "dd_date_8"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        Select(driver.find_element_by_id("dd_date_8")).select_by_visible_text("30")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedIndex | id=dd_date_8 | ]]
        Select(driver.find_element_by_id("dd_date_8")).select_by_visible_text("18")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedIndex | id=dd_date_8 | ]]
        Select(driver.find_element_by_id("dd_date_8")).select_by_visible_text("Day")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedLabel | id=dd_date_8 | ]]
        try: self.assertTrue(self.is_element_present(By.ID, "yy_date_8"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        Select(driver.find_element_by_id("yy_date_8")).select_by_visible_text("2000")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedLabel | id=yy_date_8 | ]]
        Select(driver.find_element_by_id("yy_date_8")).select_by_visible_text("1985")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedLabel | id=yy_date_8 | ]]
        Select(driver.find_element_by_id("yy_date_8")).select_by_visible_text("1950")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedLabel | id=yy_date_8 | ]]
        Select(driver.find_element_by_id("yy_date_8")).select_by_visible_text("Year")
        # ERROR: Caught exception [ERROR: Unsupported command [getSelectedLabel | id=yy_date_8 | ]]
        driver.find_element_by_id("phone_9").clear()
        driver.find_element_by_id("phone_9").send_keys("1234567890")
        try: self.assertEqual("1234567890", driver.find_element_by_id("phone_9").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("phone_9").clear()
        driver.find_element_by_id("phone_9").send_keys("")
        try: self.assertEqual("", driver.find_element_by_id("phone_9").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("Test_test")
        try: self.assertEqual("Test_test", driver.find_element_by_id("username").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("")
        try: self.assertEqual("", driver.find_element_by_id("username").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("email_1").clear()
        driver.find_element_by_id("email_1").send_keys("test@test")
        try: self.assertEqual("test@test", driver.find_element_by_id("email_1").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("email_1").clear()
        driver.find_element_by_id("email_1").send_keys("")
        try: self.assertEqual("", driver.find_element_by_id("email_1").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("description").clear()
        driver.find_element_by_id("description").send_keys("siema siema siema")
        try: self.assertEqual("siema siema siema", driver.find_element_by_id("description").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("description").clear()
        driver.find_element_by_id("description").send_keys("")
        try: self.assertEqual("", driver.find_element_by_id("description").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("password_2").clear()
        driver.find_element_by_id("password_2").send_keys("ZAQ!@wsxzaq12WSX")
        try: self.assertEqual("ZAQ!@wsxzaq12WSX", driver.find_element_by_id("password_2").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("password_2").clear()
        driver.find_element_by_id("password_2").send_keys("")
        try: self.assertEqual("", driver.find_element_by_id("password_2").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("confirm_password_password_2").clear()
        driver.find_element_by_id("confirm_password_password_2").send_keys("zaq")
        try: self.assertEqual("zaq", driver.find_element_by_id("confirm_password_password_2").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("confirm_password_password_2").clear()
        driver.find_element_by_id("confirm_password_password_2").send_keys("")
        try: self.assertEqual("", driver.find_element_by_id("confirm_password_password_2").get_attribute("value"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_name("pie_submit").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
