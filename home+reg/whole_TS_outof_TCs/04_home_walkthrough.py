# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class PrzejsciaAll(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_przejscia_all(self):
        driver = self.driver
        driver.get("http://demoqa.com/")
        try: self.assertEqual("Home", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("About us").click()
        try: self.assertEqual("About us", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Services").click()
        try: self.assertEqual("Services", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("About us").click()
        try: self.assertEqual("About us", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Draggable").click()
        try: self.assertEqual("Draggable", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("About us").click()
        try: self.assertEqual("About us", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Tabs").click()
        try: self.assertEqual("Tabs", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("About us").click()
        try: self.assertEqual("About us", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Blog").click()
        try: self.assertEqual("Blog", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("About us").click()
        try: self.assertEqual("About us", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Contact").click()
        try: self.assertEqual("Contact", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("About us").click()
        try: self.assertEqual("About us", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Registration").click()
        try: self.assertEqual("Registration", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("About us").click()
        try: self.assertEqual("About us", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Home").click()
        try: self.assertEqual("Home", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Services").click()
        try: self.assertEqual("Services", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Draggable").click()
        try: self.assertEqual("Draggable", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Services").click()
        try: self.assertEqual("Services", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Tabs").click()
        try: self.assertEqual("Tabs", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Services").click()
        try: self.assertEqual("Services", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Blog").click()
        try: self.assertEqual("Blog", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Services").click()
        try: self.assertEqual("Services", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Contact").click()
        try: self.assertEqual("Contact", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Services").click()
        try: self.assertEqual("Services", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Registration").click()
        try: self.assertEqual("Registration", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Services").click()
        try: self.assertEqual("Services", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Home").click()
        try: self.assertEqual("Home", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Draggable").click()
        try: self.assertEqual("Draggable", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Tabs").click()
        try: self.assertEqual("Tabs", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Draggable").click()
        try: self.assertEqual("Draggable", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Blog").click()
        try: self.assertEqual("Blog", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Draggable").click()
        try: self.assertEqual("Draggable", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Contact").click()
        try: self.assertEqual("Contact", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Draggable").click()
        try: self.assertEqual("Draggable", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Registration").click()
        try: self.assertEqual("Registration", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Draggable").click()
        try: self.assertEqual("Draggable", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Home").click()
        try: self.assertEqual("Home", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Tabs").click()
        try: self.assertEqual("Tabs", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Blog").click()
        try: self.assertEqual("Blog", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Tabs").click()
        try: self.assertEqual("Tabs", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Contact").click()
        try: self.assertEqual("Contact", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Tabs").click()
        try: self.assertEqual("Tabs", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Registration").click()
        try: self.assertEqual("Registration", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Demo").click()
        driver.find_element_by_link_text("Tabs").click()
        try: self.assertEqual("Tabs", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Home").click()
        try: self.assertEqual("Home", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Blog").click()
        try: self.assertEqual("Blog", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Contact").click()
        try: self.assertEqual("Contact", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Blog").click()
        try: self.assertEqual("Blog", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Registration").click()
        try: self.assertEqual("Registration", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Blog").click()
        try: self.assertEqual("Blog", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Home").click()
        try: self.assertEqual("Home", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Contact").click()
        try: self.assertEqual("Contact", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Registration").click()
        try: self.assertEqual("Registration", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Contact").click()
        try: self.assertEqual("Contact", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Home").click()
        try: self.assertEqual("Home", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Registration").click()
        try: self.assertEqual("Registration", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Home").click()
        try: self.assertEqual("Home", driver.find_element_by_css_selector("li.active > span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
