# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class WrongEntryErrors(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_wrong_entry_errors(self):
        driver = self.driver
        driver.get("http://demoqa.com/registration/")
        driver.find_element_by_id("phone_9").clear()
        driver.find_element_by_id("phone_9").send_keys("asd")
        driver.find_element_by_name("phone_9").click()
        driver.find_element_by_id("email_1").clear()
        driver.find_element_by_id("email_1").send_keys("abc")
        driver.find_element_by_name("e_mail").click()
        driver.find_element_by_id("password_2").clear()
        driver.find_element_by_id("password_2").send_keys("zaq")
        driver.find_element_by_name("password").click()
        driver.find_element_by_id("confirm_password_password_2").clear()
        driver.find_element_by_id("confirm_password_password_2").send_keys("qaz")
        driver.find_element_by_xpath("//li[14]/div/input").click()
        try: self.assertTrue(self.is_element_present(By.XPATH, "//ul[@id='pie_register']/li[6]/div/div/span"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertRegexpMatches(driver.find_element_by_xpath("//ul[@id='pie_register']/li[6]/div/div/span").text, r"^exact:[\s\S]* Minimum 10 Digits starting with Country Code$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.XPATH, "//ul[@id='pie_register']/li[8]/div/div/span"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertRegexpMatches(driver.find_element_by_xpath("//ul[@id='pie_register']/li[8]/div/div/span").text, r"^exact:[\s\S]* Invalid email address$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.XPATH, "//ul[@id='pie_register']/li[11]/div/div/span"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertRegexpMatches(driver.find_element_by_xpath("//ul[@id='pie_register']/li[11]/div/div/span").text, r"^exact:[\s\S]* Minimum 8 characters required$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.XPATH, "//ul[@id='pie_register']/li[12]/div/div/span"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertRegexpMatches(driver.find_element_by_xpath("//ul[@id='pie_register']/li[12]/div/div/span").text, r"^exact:[\s\S]* Fields do not match$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.ID, "piereg_passwordStrength"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("phone_9").clear()
        driver.find_element_by_id("phone_9").send_keys("1234567890")
        driver.find_element_by_id("email_1").clear()
        driver.find_element_by_id("email_1").send_keys("abc@abc")
        driver.find_element_by_id("password_2").clear()
        driver.find_element_by_id("password_2").send_keys("ZAQ!@wsxzaq12WSX")
        driver.find_element_by_id("confirm_password_password_2").clear()
        driver.find_element_by_id("confirm_password_password_2").send_keys("ZAQ!@wsxzaq12WSX")
        driver.find_element_by_name("pie_submit").click()
        try: self.assertTrue(self.is_element_present(By.XPATH, "//ul[@id='pie_register']/li[8]/div/div/span"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertRegexpMatches(driver.find_element_by_xpath("//ul[@id='pie_register']/li[8]/div/div/span").text, r"^exact:[\s\S]* Invalid email address$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("email_1").clear()
        driver.find_element_by_id("email_1").send_keys("abc@abc.pl")
        driver.find_element_by_name("pie_submit").click()
        try: self.assertEqual("E-mail", driver.find_element_by_xpath("//ul[@id='pie_register']/li[8]/div").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Password", driver.find_element_by_xpath("//ul[@id='pie_register']/li[11]/div").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Confirm Password", driver.find_element_by_xpath("//ul[@id='pie_register']/li[12]/div").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Phone Number", driver.find_element_by_xpath("//ul[@id='pie_register']/li[6]/div").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Mismatch", driver.find_element_by_id("piereg_passwordStrength").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
