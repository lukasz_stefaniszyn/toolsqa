# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class TabyNaHome(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_taby_na_home(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("ui-id-2").click()
        try: self.assertEqual("Content 2 Title", driver.find_element_by_css_selector("#tabs-2 > b").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("The content could contain anything text page or submit form or any other HTML objects.", driver.find_element_by_xpath("//div[@id='tabs-2']/p[2]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("ui-id-3").click()
        try: self.assertEqual("Content 3 Title", driver.find_element_by_css_selector("#tabs-3 > b").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("The content could contain anything text page or submit form or any other HTML objects.", driver.find_element_by_xpath("//div[@id='tabs-3']/p[2]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("ui-id-4").click()
        try: self.assertEqual("Content 4 Title", driver.find_element_by_css_selector("#tabs-4 > b").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("The content could contain anything text page or submit form or any other HTML objects.", driver.find_element_by_xpath("//div[@id='tabs-4']/p[2]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("ui-id-5").click()
        try: self.assertEqual("Content 5 Title", driver.find_element_by_css_selector("p > b").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("The content could contain anything text page or submit form or any other HTML objects.", driver.find_element_by_xpath("//div[@id='tabs-5']/p[2]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("ui-id-1").click()
        try: self.assertEqual("Content 1 Title", driver.find_element_by_css_selector("b").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("The content could contain anything text page or submit form or any other HTML objects.", driver.find_element_by_xpath("//div[@id='tabs-1']/p[2]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
