# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class DublicatedRegister(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_dublicated_register(self):
        driver = self.driver
        driver.get("http://demoqa.com/registration/")
        driver.find_element_by_id("name_3_firstname").clear()
        driver.find_element_by_id("name_3_firstname").send_keys("Test")
        driver.find_element_by_id("name_3_lastname").clear()
        driver.find_element_by_id("name_3_lastname").send_keys("Test")
        driver.find_element_by_name("radio_4[]").click()
        driver.find_element_by_name("checkbox_5[]").click()
        driver.find_element_by_xpath("(//input[@name='checkbox_5[]'])[2]").click()
        Select(driver.find_element_by_id("dropdown_7")).select_by_visible_text("Poland")
        Select(driver.find_element_by_id("mm_date_8")).select_by_visible_text("11")
        Select(driver.find_element_by_id("dd_date_8")).select_by_visible_text("15")
        Select(driver.find_element_by_id("yy_date_8")).select_by_visible_text("1999")
        driver.find_element_by_id("phone_9").clear()
        driver.find_element_by_id("phone_9").send_keys("1234567890")
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("t3sc1k")
        driver.find_element_by_id("email_1").clear()
        driver.find_element_by_id("email_1").send_keys("test@siema.uk")
        driver.find_element_by_id("description").clear()
        driver.find_element_by_id("description").send_keys("czesc, no siema\nhejka")
        driver.find_element_by_id("password_2").clear()
        driver.find_element_by_id("password_2").send_keys("123456789")
        driver.find_element_by_id("confirm_password_password_2").clear()
        driver.find_element_by_id("confirm_password_password_2").send_keys("123456789")
        driver.find_element_by_name("pie_submit").click()
        try: self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "p.piereg_login_error"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual("Error: Username already exists", driver.find_element_by_css_selector("p.piereg_login_error").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
