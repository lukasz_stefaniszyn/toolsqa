# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class WrongEntryErrors(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_wrong_entry_errors(self):
        driver = self.driver
        driver.get("http://demoqa.com/registration/")
        driver.find_element_by_id("phone_9").clear()
        driver.find_element_by_id("phone_9").send_keys("asd")
        driver.find_element_by_name("phone_9").click()
        driver.find_element_by_id("email_1").clear()
        driver.find_element_by_id("email_1").send_keys("abc")
        driver.find_element_by_name("e_mail").click()
        driver.find_element_by_id("password_2").clear()
        driver.find_element_by_id("password_2").send_keys("zaq")
        driver.find_element_by_name("password").click()
        driver.find_element_by_id("confirm_password_password_2").clear()
        driver.find_element_by_id("confirm_password_password_2").send_keys("qaz")
        driver.find_element_by_xpath("//li[14]/div/input").click()
        try: self.assertTrue(self.is_element_present(By.CLASS_NAME, "legend_txt"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertRegexpMatches(driver.find_element_by_class_name("legend_txt").text, r"^exact:[\s\S]* Minimum 8 characters required$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertRegexpMatches(driver.find_element_by_class_name("legend_txt").text, r"^exact:[\s\S]* Fields do not match$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertTrue(self.is_element_present(By.ID, "piereg_passwordStrength"))
        except AssertionError as e: self.verificationErrors.append(str(e))
            
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
