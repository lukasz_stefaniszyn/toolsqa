# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

import unittest, time, re
from time import sleep
from selenium.webdriver.support.expected_conditions import presence_of_element_located

class WrongEntryErrors(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.maximize_window()
        self.driver.implicitly_wait(1)
        self.base_url = "http://demoqa.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
        
    def get_element_or_wait(self, how, what):
        """
        Utility method to get element on page until timeout
        :params how: By locator type
        :params what: locator value
        """
        element = None;
        try: 
            element = WebDriverWait(self.driver, timeout=1)\
                        .until(presence_of_element_located( (how, what) ))                        
        except TimeoutException, e:
            pass;
        return element

    
    def test_wrong_entry_errors(self):
        driver = self.driver
        driver.get("http://demoqa.com/registration/")
# ponizej mam wrzucone zdefiniowany name locator do ktorego podstawione mam "compund name" classy, ktora nie jest dozwolona dla pythona
# konstrukcja "..." wynika z DOMa strony <div class="legend_txt"> -> <span class="legend error"> 
        __name_locator = \
            "span[class='legend error']"
        driver.find_element_by_id("phone_9").clear()
        driver.find_element_by_id("phone_9").send_keys("asd")
#         ActionChains(driver).click("email_1");
        
        
#         driver.find_element_by_name("phone_9").click()
        driver.find_element_by_id("email_1").clear()
        driver.find_element_by_id("email_1").send_keys("abc")
        driver.find_element_by_name("e_mail").click()
        driver.find_element_by_id("password_2").clear()
        driver.find_element_by_id("password_2").send_keys("zaq")
         
        
#         driver.find_element_by_name("password").click()
        

        

        
        
        driver.find_element_by_id("confirm_password_password_2").clear()
        
        
        
        driver.find_element_by_id("confirm_password_password_2").send_keys("qaz")


#         driver.find_element_by_xpath("//li[14]/div/input").click()
        driver.execute_script("window.scrollBy(0,400)");
        
        element = self.get_element_or_wait(By.CSS_SELECTOR, __name_locator);
        if element is not None:
            self.assertRegexpMatches(element.text, r"^exact:[\s\S]* * Minimum 8 characters required$");
            
        driver.find_element_by_name("pie_submit").click()
        
        
        
# ponizej prawidlowe zastosowanie "__name_locator"        
        
        
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, __name_locator))
# ponizej prawidlowe zastosowanie "__name_locator"
        try: self.assertRegexpMatches(driver.find_element_by_css_selector(__name_locator).text, r"^exact:[\s\S]* * Minimum 8 characters required$")
        except AssertionError as e: self.verificationErrors.append(str(e))

#    #post-49 > div > p > strong:nth-child(13) password
#    #post-49 > div > p > strong:nth-child(5)
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
