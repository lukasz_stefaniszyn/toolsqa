# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class OBRAZKI(unittest.TestCase):
    def setUp(self):
#        self.driver = webdriver.Firefox()
        self.driver = webdriver.Chrome(r"C:\chromedriver_win32\chromedriver.exe")
        self.driver.implicitly_wait(30)
        self.base_url = "http://demoqa.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_o_b_r_a_z_k_i(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        # ERROR: Caught exception [ERROR: Unsupported command [selectWindow | null | ]]
        try: self.assertTrue(self.is_element_present(By.XPATH, "//img[@alt='pattern-14']"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("//img[@alt='pattern-14']").click()
        driver.back()
        try: self.assertTrue(self.is_element_present(By.XPATH, "(//img[@alt='pattern-14'])[2]"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("(//img[@alt='pattern-14'])[2]").click()
        driver.back()
        try: self.assertTrue(self.is_element_present(By.XPATH, "(//img[@alt='pattern-14'])[3]"))
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("(//img[@alt='pattern-14'])[3]").click()
        driver.back()
        driver.get("http://demoqa.com/wp-content/uploads/2014/08/pattern-14.png")
        driver.back()
        self.assertTrue(self.is_element_present(By.XPATH, "//img[@alt='pattern-14']"))
        self.assertTrue(self.is_element_present(By.XPATH, "(//img[@alt='pattern-14'])[2]"))
        self.assertTrue(self.is_element_present(By.XPATH, "(//img[@alt='pattern-14'])[3]"))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
